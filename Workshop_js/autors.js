
'use strict';

const autoresKey = 'autores';
var x = 20;

function insertBook() {
	const nombre = document.getElementById('nombre').value;
	const apellido = document.getElementById('apellido').value;
	const edad = document.getElementById('edad').value;
	let currentKey = localStorage.getItem('booksLastInsertedId');

	if (!currentKey) {
		localStorage.setItem('booksLastInsertedId', 1);
		currentKey = 1;
	} else {
		currentKey = parseInt(currentKey) + 1;
		localStorage.setItem('booksLastInsertedId', currentKey);
	}

	// create the book object
	const autor = {
		nombre,
		apellido,
			edad,
		id: currentKey
	};

	// add it to the database
	let autores = JSON.parse(localStorage.getItem(autoresKey));
	if (autores && autores.length > 0) {
		autores.push(autor);
	} else {
		autores = []
		autores.push(autor)
	}
	localStorage.setItem(autoresKey, JSON.stringify(autores));

	clearFields();
	// render the books
	renderTable('autores', autores);
}


function saveBook() {
	const nombre = document.getElementById('editNombre').value;
	const apellido = document.getElementById('editApellido').value;
	const edad = document.getElementById('editEdad').value;
	const id = document.getElementById('editId').value;

	// create the book object
	const autor = {
		nombre,
		apellido,
		edad,
		id
	};
	debugger;
	// add it to the database
	let autores = JSON.parse(localStorage.getItem(autoresKey));
	let results = autores.filter(autor => autor.id != id);
	results.push(autor);
	localStorage.setItem(autoresKey, JSON.stringify(results));

	clearFields();
	// render the books
	renderTable('autores', autores);
}

function clearFields() {
	document.getElementById('nombre').value = '';
	document.getElementById('apellido').value = '';
	document.getElementById('edad').value = '';
}


/**
 * Renders an HTML table dinamically
 *
 * @param tableName
 * @param tableData
 */
function renderTable(tableName, tableData) {
	let table = jQuery(`#${tableName}_table`);
	// loop through all the items of table and generates the html
	let rows = "";
	tableData.forEach((autor, index) => {
		let row = `<tr><td>${autor.nombre}</td><td>${autor.apellido}</td><td>${autor.edad}</td>`;
		row += `<td> <a onclick="editEntity(this)" data-id="${autor.id}" data-entity="${tableName}" class="link edit">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${autor.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
		rows += row + '</tr>';
	});
	table.html(rows);
}

function editEntity(element) {
	debugger;
	const dataObj = jQuery(element).data();

	let autores = JSON.parse(localStorage.getItem(autoresKey));
	let autorFound;
	autores.forEach(function (autor) {
		if (autor.id == dataObj.id) {
			autorFound = autor;
			return;
		}
	});

	document.getElementById('editNombre').value = autorFound.nombre;
	document.getElementById('editApellido').value = autorFound.apellido;
		document.getElementById('editEdad').value = autorFound.edad;
	document.getElementById('editId').value = autorFound.id;
}

function deleteEntity(element) {
	if (confirm('Are you sure you want to delete?')) {
		const dataObj = jQuery(element).data();
		// const newEntities = deleteFromTable(dataObj.entity, dataObj.id);

		let autores = JSON.parse(localStorage.getItem(autoresKey));
		let results = autores.filter(autor => autor.id != dataObj.id);
		localStorage.setItem(autoresKey, JSON.stringify(results));
		renderTable(dataObj.entity, results);
	}
}


/*function deleteEntity(element) {
	const dataObj = jQuery(element).data();
	const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
	renderTable(dataObj.entity, newEntities);
}*/

function loadTableData(tableName) {
	renderTable(tableName, getTableData(tableName));
}

function getTableData(tableName) {
  let tableData = JSON.parse(localStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  return tableData;
}


/**
 * Binds the different events to the different elements of the page
 */
function bindEvents() {
	jQuery('#add-book-button').bind('click', function (element) {
		insertBook();
	});

	jQuery('#save-book-button').bind('click', function (element) {
		saveBook();
	});
}
